# This playbook is not meant to be run directly with ansible-playbook.
# It *can* be, but you'll have to set all the environment variables by
# hand.  Instead you should run `bin/create`, which will set some
# defaults and prompt you for passwords and other parameters; it can
# also read a file containing settings from a previous play.

- name: Hydra setup
  # https://support.ansible.com/hc/en-us/articles/201957947-How-do-I-access-shell-environment-variables-
  remote_user: "{{ lookup('env', 'USER') }}"
  hosts: all

  vars:
    deploy_user: "{{ lookup('env', 'DEPLOY_USER') }}"
    deploy_group: "{{ lookup('env', 'DEPLOY_GROUP') }}"
    keyfile: "{{ lookup('env', 'IDENTITY') }}.pub"

    # On production and vagrant instances, project_dir will be /opt/
    # and project_name will be hydra
    project_dir: "{{ lookup('env', 'PROJECT_DIR') }}"
    project_name: hydra
    install_path: "{{ project_dir }}/install"

    server_name: "{{ lookup('env', 'SERVER') }}"
    rails_env: "{{ lookup('env', 'RAILS_ENV') }}"
    rails_secret_key_base: "{{ lookup('env', 'SECRET') }}"

    # Path to Rails' config directory; on production and vagrant it's
    # project_dir/project_name/shared/config, and on OSX it's
    # project_dir/project_name/config
    config_path: "{{ lookup('env', 'CONF_PATH') }}"

    # `tomcat` is the directory in the Tomcat installation where
    # servlets, libraries and configuration files can be installed
    tomcat: "{{ lookup('env', 'TOMCAT') }}"

    # On production and vagrant instances, this will be
    # project_dir/solr; on OSX this will be prefix/example/solr in
    # the Homebrew installation
    solr_home: "{{ lookup('env', 'SOLR_HOME') }}"

    ansible_user: "{{ lookup('env', 'REMOTE_USER') }}"
    ansible_group: "{{ lookup('env', 'REMOTE_GROUP') }}"

    # Password for the 'postgres' user
    pg_pass: "{{ lookup('env', 'PG_PASS') }}"

    hydra_db: "{{ lookup('env', 'HYDRA_DB') }}"
    hydra_pg_user: hydra
    hydra_pg_pass: "{{ lookup('env', 'H_PG_PASS') }}"

    fedora_pass: "{{ lookup('env', 'FEDORA_PASS') }}"

    ruby_version: 2.3.0
    # corresponds to the name of the versioned directory in gems
    ruby_abi: 2.3
    ruby_sha: ba5ba60e5f1aa21b4ef8e9bf35b9ddb57286cb546aac4b5a28c71f459467e507

  roles:
    # Install supporting software, create users/directories
    - { role: prereqs }

    # Configure PostgreSQL and Redis
    - { role: databases }

    # Install Passenger, configure Apache and Tomcat
    - { role: servers }

    # Set up a basic application configuration on top of Hydra
    - { role: application,
        localhost_port: "8484",
        tomcat_port: "8080",
        when: ansible_system == "Linux" }
    - { role: application,
        localhost_port: "3000",
        tomcat_port: "8080",
        when: ansible_system == "Darwin" }

    # Configure sshd
    - { role: cleanup }
