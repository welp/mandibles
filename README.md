# Mandibles

This is an Ansible playbook to set up a stock
[Hydra](https://github.com/projecthydra/hydra-head) instance.

# Running a development instance on Mac OSX

1. `bin/hydrate local`

    If you don’t have Homebrew installed, you will be asked for
    permission to download and run a bootstrap script that will
    install Homebrew and other development tools.

2. `cd hydra && bin/rails server`

3. The following services should be running:

    - WEBrick: <http://localhost:3000/> (`bin/rails server`)

    - Tomcat: <http://localhost:8080/> (`tomcat/bin/catalina.sh [stop|start]`)

        - Solr: <http://localhost:8080/solr/>

        - Fedora: <http://localhost:8080/fedora/>

    - PostgreSQL: <http://localhost:5432> (`brew services restart postgresql`)

# Running a development instance in Vagrant

## Prerequisites

- Vagrant 1.7.2 or higher (`brew tap Caskroom/cask && brew cask install vagrant`)
- VirtualBox 4.3.30 or higher (`brew tap Caskroom/cask && brew cask install virtualbox`)

1. Download a [minimal Centos 7 disk image](https://wiki.centos.org/Download).

2. Run the setup script: `bin/build <path/to/disk-image> bin/ks.cfg`

3. Register the CentOS box with Vagrant: `vagrant box add boxes/centos70-x86_64.box --name centos7`

4. `bin/hydrate development`

    Once the VM is created, you can SSH into it with `vagrant ssh` or
    manually by using the config produced by `vagrant ssh-config`.

5. `cd hydra && make vagrant` to deploy with Capistrano

6. The following services should be running; `sudo service [program] restart` if not:

    - Apache/Passenger (httpd): http://localhost:8484/

    - Tomcat: <http://localhost:2424/>

        - Solr: <http://localhost:2424/solr/>

        - Fedora: <http://localhost:2424/fedora/>

    - PostgreSQL: <http://localhost:5432>

# Provisioning and deploying to production

## Prerequisites

- 4GB+ RAM on the server

## Steps

2. `bin/hydrate production` to provision the production server

    It’s (relatively) safe to set `REMOTE_USER` as root, since a
    non-root `deploy` user will be created for Capistrano and root SSH
    access will be blocked once provisioning is complete.

3. `cd hydra && SERVER=<hostname> REPO=<repo.git> make prod` to deploy with Capistrano.

# Troubleshooting

- **mod_passenger fails to compile**: There’s probably not enough memory on the server.

- **`SSHKit::Command::Failed: bundle exit status: 137` during `bundle install`**: Probably not enough memory.

- **Nokogiri fails to compile**: Add the following to `config/deploy.rb`:

    ```ruby
    set :bundle_env_variables, nokogiri_use_system_libraries: 1
    ```

- **Passenger fails to spawn process**

    ```
    [ 2015-11-26 01:56:19.7981 20652/7f16c6f19700 App/Implementation.cpp:303 ]: Could not spawn process for application /opt/hydra/current: An error occurred while starting up the preloader: it did not write a startup response in time.
    ```

    Try restarting Apache and deploying again.

- **Timeout during assets precompile**:  Not sure yet!
