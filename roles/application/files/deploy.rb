# config valid only for current version of Capistrano
lock "3.4.0"

set :application, "hydra"
set :scm, :git
set :repo_url, ENV["REPO"]
set :deploy_to, "/opt/hydra"

set :stages, %w(production vagrant)
set :default_stage, "vagrant"

set :log_level, :debug
set :bundle_flags, "--deployment"
set :bundle_env_variables, nokogiri_use_system_libraries: 1
set :passenger_restart_with_touch, true

set :keep_releases, 5
set :assets_prefix, "#{shared_path}/public/assets"

set :linked_dirs, %w(
  tmp/pids
  tmp/cache
  tmp/sockets
  public/assets
  config/environments
)

set :linked_files, %w(
  config/blacklight.yml
  config/database.yml
  config/environment.rb
  config/fedora.yml
  config/secrets.yml
  config/solr.yml
  config/initializers/assets.rb
)
SSHKit.config.command_map[:rake] = "bundle exec rake"

# Default branch is :master
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call
